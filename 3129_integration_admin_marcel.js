import * as admin from '../../roles/Admin';
import MenuPageObject from '../../pageobjects/admin/generic/MenuPageObject';
import PathwayPageObject from '../../pageobjects/admin/structure/pathway/PathwayPageobject';
import PhasesPageobject from '../../pageobjects/admin/structure/phases/PhasesPageobject';
import PhaseAddWaypointPageobject from '../../pageobjects/admin/structure/phases/PhaseAddWaypointPageobject';
import WaypointPageobject from '../../pageobjects/admin/structure/waypoint/WaypointPageobject';
import VariantPageobject from '../../pageobjects/admin/structure/variant/VariantPageobject';
import PackagesPageobject from '../../pageobjects/admin/application/PackagesPageobject';
import { Selector, t, ClientFunction } from 'testcafe';

const menuPageObject = new MenuPageObject();
const pathwayPageObject = new PathwayPageObject();
const phasesPageobject = new PhasesPageobject();
const phaseAddWaypointPageobject = new PhaseAddWaypointPageobject();
const waypointPageobject = new WaypointPageobject();
const variantPageobject = new VariantPageobject();

const packagesPageobject = new PackagesPageobject();

fixture `E2E Test - Structure and Manage Pathways, Phases & Waypoints`
    .beforeEach(async t =>{
        await t.useRole(admin.genericAdmin)
        // await menuPageObject.clickMenuItem('Applicatie', 'Pakketten');
        // await t.click(packagesPageobject.clearCashes);
    });

 test('edit button presence on the Haltes criteria' , async t=> {
    await t.click(Selector('a').withText('Structuur'))
    await t.click(Selector('a').withText('Halte criteria'))
    await t.click(Selector('a').withText('Aanpassen'))
    await t.expect(Selector('a.edit-icon')).ok()
})

 test('check if the right waypoint is selected', async t=> {
    await t.click(Selector('a').withText('Structuur'))
    await t.click(Selector('a').withText('Halte criteria'))
    await t.click(Selector('a').withText('Aanpassen'))
    await t.click(Selector('a.edit-icon'))
    await t.expect(Selector('h2').withText('Halte')).ok()
})


test('check if the right waypoint is selected', async t=> {
    await t.click(Selector('a').withText('Structuur'));
    await t.click(Selector('a').withText('Halte criteria'));
    await t.click(Selector('a').withText('Aanpassen'));
    var val1 = await t.eval(() => document.querySelector('.criteria-list tbody tr:first-of-type td:first-of-type').innerHTML);
    await t.click(Selector('a.edit-icon'));
    var val2 = await t.eval(() => document.querySelector('.waypoint-list tbody tr:first-of-type td:first-of-type').innerHTML);
    await t.expect(val2).eql(val1);
});

//This test is shit... I cant make it write stuff in the text field
test('edit a Halte name and see if the change is saved', async t=> {
    await t.click(Selector('a').withText('Structuur'));
    await t.click(Selector('a').withText('Halte criteria'));
    await t.click(Selector('a').withText('Aanpassen'));
    await t.click(Selector('a.edit-icon'));
    //await t.click(Selector('input.form-control'));
    await t.typeText(Selector('input.form-control').nth(0), 'edited', { replace: true });
    await t.wait(1000);
    await t.click(Selector('button').withText('Opslaan'));
    var val1 = await t.eval(() => document.querySelector('.waypoint-list tbody tr:first-of-type td:first-of-type').innerHTML);
    await t.keyPress('esc');
    var val2 = await t.eval(() => document.querySelector('.criteria-list tbody tr:first-of-type td:first-of-type').innerHTML);
    await t.expect(val2).eql(val1);


})